	// spits out an error message
	onError = function(tx, error) {
		document.getElementById('error').innerHTML = error.message;
	}

	// this is the function that searches the SQL and actually spits out the records
	displayRecords = function() {		
		// declare variables
		var valid = true;
		var searchRestaurants = true;

		var businessName = document.getElementById("businessName").value;
		if (businessName == ""){ 
			searchRestaurants = false;
			document.getElementById('error').innerHTML = "<span style='color: red'>Please enter a value in the box above.</span>";
		}
				
		// this if is so it doesn't unecessarily trigger every time
		if (searchRestaurants){
			var db = openDatabase("com.pearprogramming.fooddisclosure.db", "", "Health Check", 15*1024*1024);  // 10 mb database. Look to reduce in the future
			var selectStatement = "SELECT DISTINCT FACILITY_ID, BUSINESS_NAME, ADDRESS, PHONE"
								+ " FROM food_disclosure" 
								+ " WHERE BUSINESS_NAME LIKE '%" + businessName + "%'";
			db.transaction(function (transaction) {
				transaction.executeSql(selectStatement,
					[],
					function (transaction, results) { onGetAllRetaurantsSuccess(results) },
					onError);
			});
		}// if (searchRestaurants)
	}  //displayrecords bracket

	getAll = function(){
		var db = openDatabase("com.pearprogramming.fooddisclosure.db", "", "Health Check", 15*1024*1024);  // 10 mb database. Look to reduce in the future
			var selectStatement = "SELECT DISTINCT FACILITY_ID, BUSINESS_NAME, ADDRESS, PHONE"
								+ " FROM food_disclosure"
								+ " ORDER BY BUSINESS_NAME asc";
			db.transaction(function (transaction) {
				transaction.executeSql(selectStatement,
					[],
					function (transaction, results) { onGetAllRetaurantsSuccess(results) },
					onError);
			});
	}
	
	onGetAllRetaurantsSuccess = function(results) { 
		document.getElementById('dataList').innerHTML = ""; 
		document.getElementById('error').innerHTML = "";

		var error = document.getElementById("error");
		var aRow = null;
		
		// if no records are found, tells user
		if (results.rows.length == 0) {
			error.innerHTML = '<h4>No records found. Please ensure that correct information was entered.</h4>';
			document.getElementById('error').innerHTML = "";
		}
		
		 //read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);

			// outputs it neatly to a listview
			listItem = document.createElement('div');
			listItem.setAttribute('id', 'menuItem');
			listItem.setAttribute('data-bb-type', 'item');
			listItem.setAttribute('data-bb-title', aRow['BUSINESS_NAME']);
			listItem.setAttribute('onclick', 'restaurantDetail(' + aRow['PHONE'] + ');');
			listItem.innerHTML = aRow['ADDRESS'];
			
			dataList.appendItem(listItem);  //Append the item
		}
	}  //onGetAllRetaurantsSuccess bracket
	
	restaurantDetail = function(PHONE) {	
		var db = openDatabase("com.pearprogramming.fooddisclosure.db", "", "Health Check", 15*1024*1024);	
		var selectStatement = "SELECT BUSINESS_NAME, ADDRESS, PHONE, INFRACTION, INSPECTION_DATE, INFRACTION_TYPE, INFRACTION_DETAIL" 
							+ " FROM food_disclosure" 
							+ " WHERE PHONE = '" + PHONE + "'"
							
		db.transaction(function (transaction) {
			transaction.executeSql(selectStatement,
				[],
				function (transaction, results) { onGetAllDetailsSuccess(results) },
				onError);
		});
	}// if (searchRestaurants)

	onGetAllDetailsSuccess = function(results) { 
		document.getElementById('dataList').innerHTML = ""; 
		document.getElementById('error').innerHTML = "";
		document.getElementById('titleName').innerHTML = "";
		var error = document.getElementById("error");
		var aRow = null;
		
		// if no records are found, tells user
		if (results.rows.length == 0) {
			error.innerHTML = '<h4>No records found. Please ensure that correct information was entered.</h4>';
			document.getElementById('error').innerHTML = "";
		} //if
		
		//Displays business name under search button
		aRow = results.rows.item(0);
		titleName.innerHTML = "<h4>"
		+ aRow['BUSINESS_NAME']
		+ "<br />Address: "
		+ aRow['ADDRESS']
		+ "<br />Phone: "
		+ aRow['PHONE'];
		
		 //read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);
			
			dataList.innerHTML += "<p style='border-top:5px solid'><br />Date of inspection: "
			+ aRow['INSPECTION_DATE']
			+ "<br />"
			+ "Infraction Type: "
			+ aRow['INFRACTION_TYPE']
			+ "<br />"
			+ "Infraction: <br />"
			+ aRow['INFRACTION']
			+ "<br />"
			+ "Infraction Details: <br />"
			+ aRow['INFRACTION_DETAIL']
			+ "</p>";
		} //for
	}  //onGetAlDetailsSuccess bracket




	 