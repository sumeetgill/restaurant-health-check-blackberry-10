displayInfo = function(){
	var output = document.getElementById("output");
	var selected = document.getElementById("selections").value;
	var appname = blackberry.app.name;
	var appversion = blackberry.app.version;
	var appauthor = blackberry.app.author;
	
	output.innerHTML = "";
	
	if(selected == "App Info"){
		output.innerHTML +=  "<p>All data provided and owned by the "
			+ "<a href='http://www.grt.ca/en/doingBusiness/supportingsoftwareapplications.asp?_mid_=18101' target='_blank'>Region of Waterloo.</a>"
		+ "</p><br />";
		output.innerHTML += appname + ": " + appversion + "<br>"; 
	}else if(selected == "About Us"){
		output.innerHTML += "Developed By:<ul align='left'><li>Paul Jacques (UI)</li><li>Sumeet Gill (Back End)</li></ul>"; 
	}else if(selected == "Contact Us"){
		output.innerHTML = "Please feel free to send an email with any questions, features you would like to see and/or bug submissions. <br /><br /><br />" + 
		"<a href='mailto:contact@pearprogramming.biz'>contact@pearprogramming.biz</a>";
	}else if(selected == "Tutorial"){
		displayHelp();
	}
}

specialfeature = function(){
	alert("coming soon");
}

displayHelp = function(){
	var output = document.getElementById("output");
	output.innerHTML = "";
	
		output.innerHTML += "<h4>Maps</h4>"
		+ "<ul align='left'>"
			+ "<li>From home, select Maps. From this page, you will be able to find a specific restaurant.</li>"
			+ "<li>To Zoom in, pinch out on map</li>"
			+ "<li>To Zoom out, pinch in on map</li>"
			+ "<li>Click on the restaurant you are wanting. Here you will find the the restaurant details</li>"
			+ "<li>Press back button</li>"
		+ "</ul><br />"
		+ "<h4>Search Restaurants</h4>"
		+ "<ul align='left'>"
			+ "<li>From home, select Search. Here, you will be able to find restaurants by name</li>"
			+ "<li>Enter in the restaurant name you're interested in searching</li>"
			+ "<li>You can also scroll through the list to find a restaurant</li>"
			+ "<li>After entering in your desired restaurant, click the Search Restaurants button</li>"
			+ "<li>A list will appear showing all restaurants in the Region of Waterloo with that name by address</li>"
			+ "<li>Click the desired location</li>"
			+ "<li>A list will appear with all infractions this restaurant has had for up to the past 3 years</li>"	
		+ "</ul><br />"
		+ "<h4>Special Features</h4>"
		+ "<ul align='left'>"
			+ "<li>Here you will be able to find the upcoming feature using NFC technology</li>"
		+ "</ul><br />"
		+ "<h4>Other</h4>"
		+ "<ul align='left'>"
			+ "<h4>Select an option from the drop down<br />About the App</h4>"
			+ "<ul align='left'>"
				+ "<li>Information about the app, including app version</li>"
			+ "</ul><br />"
			+ "<h4>About Us</h4>"
			+ "<ul align='left'>"
				+ "<li>Information about the developers</li>"
			+ "</ul><br />"
			+ "<h4>Contact Us</h4>"
			+ "<ul align='left'>"
				+ "<li>Email address and website of the developers</li>"
			+ "</ul>";
		+ "</ul><br />";
		+ "<h4>Settings</h4>"
		+ "<ul align='left'>"
			+ "<li>You can find settings here: <a href='settings.htm'>Settings</a></li>"
			+ "<li>If your database becomes corrupted, press the Reset Database button </li>"
			+ "<li>Once the button is pressed, on next load of the application, the database will reinstall itself</li>"
		+ "</ul><br />";
}