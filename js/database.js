// create database if does not exist
initializeDatabase = function(){
	try {
		html5sql.openDatabase(
		"com.pearprogramming.fooddisclosure.db",
		"Health Check",
		15*1024*1024); // 15 mb database. Leaving room for improvement
		
		
		// creates the route table
		$.get('food_disclosure.sql',function(sql){
			html5sql.process(sql,
			function(){	
				localStorage.setItem('HealthTable', "Created");
				bb.pushScreen('home.htm', 'home');
			},
			function(error, failingQuery){
				document.getElementById('output').innerHTML = "Error: "+ error.message + " when processing " + failingQuery;
			}
			);	
		});
	}
	catch (error) {
		$("#results").text("Error: " + error.message);
	}
} // initialize database

