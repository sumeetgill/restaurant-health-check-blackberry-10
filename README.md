# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* I created this app in school to allow users to check the health records of the restaurants they were going to. It uses data provided by the region of waterloo.
* Version 1.0

### How do I get set up? ###

#### Summary of set up ####

* Download the files and run it in the BB10 ripple simulator. You can also package it and sideload it onto a BB10 phone.

#### Configuration ####

* If you wanted to use it today, you would need to load new SQL files with updated health and safety records.

#### Dependencies ####

* Completely dependent on GTFS data provided by the city via Google.

####Deployment instructions####

* Check configuration above.

### Contribution guidelines ###

I don't have guidelines because I don't really expect anyone to contribute to this. It would probably be better to use an off-device data source and write it natively.

### Who do I talk to? ###

* Me! Sumeetgill